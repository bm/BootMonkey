/*
 * Copyright (C) 2017-2020 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>
#include <stdlib.h>

#include "util/file.h"
#include "util/utf8.h"

#include "ast.h"
#include "debug.h"
#include "lexer.h"
//#include "parser.h"


static void display_usage(int argc, char **argv)
{
	char *prog_name = "bmc";

	if (argc >= 1)
		prog_name = argv[0];

	fprintf(stderr, "Usage: %s <file.bm>\n", prog_name);
}

int main(int argc, char **argv)
{
	int status = 1;
	int result;
	const char *in_filename;
	struct u_file in_file;
	char *in_contents = NULL;
	size_t in_size = 0;
	struct u_utf8_ctx utf8_ctx;
	size_t utf8_chk_off;
	//int parser_status = -1;
	struct lexer_context lex;
	//struct bm_ast ast;

	if (argc != 2)
	{
		display_usage(argc, argv);
		return 1;
	}

	in_filename = argv[1];

	/* Internal init */
	lexer_init();

	/* IO */
	result = u_file_open(&in_file, in_filename, U_FILE_OPEN_READ | U_FILE_OPEN_BIN);
	if (result < 0)
	{
		fprintf(stderr, "Failed to open \"%s\" for reading.\n", in_filename);
		return 1;
	}

	in_contents = u_file_get_contents(&in_file, &in_size);
	if (!in_contents)
	{
		if (in_size == U_FILE_SIZE_INVAL)
			fprintf(stderr, "Failed to read from \"%s\".\n", in_filename);
		else
			fprintf(stderr, "Input file \"%s\" is empty.\n", in_filename);

		goto free_exit;
	}

	u_utf8_init(&utf8_ctx, in_contents, in_size);

	result = u_utf8_check(&utf8_ctx, &utf8_chk_off);
	if (result < 0)
	{
		fprintf(stderr, "Malformed UTF-8 character at offset %zu of input file \"%s\": '\\x%02X'.\n", utf8_chk_off, in_filename, (unsigned char)in_contents[utf8_chk_off]);
		goto free_exit;
	}

	printf("%s\n", in_contents);

	lexer(&lex, in_contents, in_size);

#if 1
	do
	{
		struct bm_token tok;

		result = lexer_next(&lex, &tok);
		if (result == 0)
		{
			printf("%s(%u)(%zu:%zu) ", debug_token_type(tok.type), tok.type, tok.lineno, tok.colno);
			if (tok.type == BM_TOK_EOF || tok.type == BM_TOK_EOL)
				printf("\n");
			lexer_token_deinit(&tok);
		}
	} while (result == 0);
#endif

#if 0
	parser_status = parser(&lexer_out, &ast);
	if (parser_status < 0)
	{
		status = parser_status;
		goto free_exit;
	}

	parser_debug_ast(&ast);
#endif
	status = 0;

free_exit:
//	if (parser_status == 0)
//		parser_destroy(&ast);

	if (in_contents)
		free(in_contents);

	u_file_close(&in_file);

	return status;
}
