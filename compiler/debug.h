/*
 * Copyright (C) 2018-2020 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef DEBUG_H
#define DEBUG_H 1

#include "ast.h"
#include "ast_node_type.h"
#include "token.h"

extern const char *debug_token_type(enum bm_token_type type);
extern const char *debug_node_type(enum bm_ast_node_type type);
extern const char *debug_visibility(enum bm_ast_visibility visibility);
extern const char *debug_annotation_type(enum bm_ast_annotation_type type);
extern const char *debug_val_imm_type(enum bm_ast_val_imm_type type);
extern const char *debug_bool(enum bm_ast_bool b);

#endif
