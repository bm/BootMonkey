/*
 * Copyright (C) 2017-2020 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util/ascii.h"
#include "util/macro.h"
#include "util/misc.h"
#include "util/utf8.h"
#include "token.h"
#include "lexer.h"
#include "utils.h"

struct token_handler;

typedef int (*token_handler_callback)(struct lexer_context *ctx, struct token_handler *params, struct bm_token *token);

struct token_handler
{
	unsigned int precedence;
	enum bm_token_type type;
	const char *keyword;
	size_t keyword_len;
	token_handler_callback callback;
};

static inline int is_identifier_first_char(char c)
{
	return c == '_' || u_ascii_isalpha(c);
}

static inline int is_identifier_char(char c)
{
	return is_identifier_first_char(c) || u_ascii_isdigit(c);
}

static inline int is_identifier_lalnum_first_char(char c)
{
	return u_ascii_islower(c);
}

static inline int is_identifier_lalnum_char(char c)
{
	return u_ascii_islower(c) || u_ascii_isdigit(c);
}

static inline int is_identifier_lsnake_first_char(char c)
{
	return u_ascii_islower(c);
}

static inline int is_identifier_lsnake_char(char c)
{
	return u_ascii_isalpha(c) || u_ascii_isdigit(c);
}

static inline int is_identifier_ucamel_first_char(char c)
{
	return u_ascii_isupper(c);
}

static inline int is_identifier_ucamel_char(char c)
{
	return u_ascii_isalpha(c) || u_ascii_isdigit(c);
}

static int lexer_token_init(struct bm_token *token, enum bm_token_type type)
{
	token->type = type;
	return 0;
}

static int lexer_token_init_int(struct bm_token *token, enum bm_token_type type, int val)
{
	token->type = type;
	token->data.as_int = val;
	return 0;
}

static int lexer_token_init_float(struct bm_token *token, enum bm_token_type type, float val)
{
	token->type = type;
	token->data.as_float = val;
	return 0;
}

static int lexer_token_init_char(struct bm_token *token, enum bm_token_type type, char c)
{
	token->type = type;
	token->data.as_char = c;
	return 0;
}

/* Store an already malloc-ed str as token data */
static int lexer_token_init_str(struct bm_token *token, enum bm_token_type type, char *str)
{
	token->type = type;
	token->data.as_str = str;
	return 0;
}

static int lexer_token_init_str_dup(struct bm_token *token, enum bm_token_type type, const char *str, size_t max_len)
{
	char *s;

	s = u_strndup(str, max_len);
	if (!s)
		return -1;

	return lexer_token_init_str(token, type, s);
}

static void lexer_token_set_pos(struct lexer_context *ctx, struct bm_token *token)
{
	token->off = ctx->off;
	token->lineno = ctx->lineno;
	token->colno = ctx->colno;
}

int lexer_token_deinit(struct bm_token *token)
{
	switch (token->type)
	{
		case BM_TOK_COMMENT:
		case BM_TOK_IDENTIFIER:
		case BM_TOK_IMM_STR:
			free(token->data.as_str);

		default:
			break;
	}

	token->type = BM_TOK_UNKNOWN;
	return 0;
}

/* Update ctx to new cur, but do not take account of EOLs */
static void ctx_upd(struct lexer_context *ctx, const char *new_cur)
{
	size_t size = (size_t)(new_cur - ctx->cur);
	size_t char_count = u_utf8_fast_length(ctx->cur, size);

	ctx->cur += size;
	ctx->off += size;
	ctx->colno += char_count;
}

static void skip_spaces(struct lexer_context *ctx)
{
	const char *s = ctx->cur;
	const char *end = ctx->end;

	/* EOL must not be skipped */
	while (s < end && u_ascii_isspace(*s) && *s != '\n')
		s++;

	ctx_upd(ctx, s);
}

static int match_simple_keyword(struct lexer_context *ctx, struct token_handler *params, struct bm_token *token)
{
	const char *cur = ctx->cur;
	size_t keyword_len;

	keyword_len = u_min2_size_t(params->keyword_len, (size_t)(ctx->end - cur));
	if (!keyword_len || strncmp(ctx->cur, params->keyword, keyword_len) != 0)
		return -1;

	cur += keyword_len;

	lexer_token_init(token, params->type);
	lexer_token_set_pos(ctx, token);

	ctx_upd(ctx, cur);
	return 0;
}

static int match_eol(struct lexer_context *ctx, struct token_handler *params, struct bm_token *token)
{
	int result;

	result = match_simple_keyword(ctx, params, token);
	if (result < 0)
		return result;

	ctx->line = ctx->cur;
	ctx->lineno++;
	ctx->colno = 1;
	return 0;
}

static int match_isolated_keyword(struct lexer_context *ctx, struct token_handler *params, struct bm_token *token)
{
	const char *cur = ctx->cur;
	size_t keyword_len;

	if (cur > ctx->line && is_identifier_char(cur[-1]))
		return -1;

	keyword_len = u_min2_size_t(params->keyword_len, (size_t)(ctx->end - cur));
	if (strncmp(cur, params->keyword, keyword_len) != 0)
		return -1;

	cur += keyword_len;

	if (*cur && is_identifier_char(*cur))
		return -1;

	lexer_token_init(token, params->type);
	lexer_token_set_pos(ctx, token);

	ctx_upd(ctx, cur);
	return 0;
}

static int match_comment(struct lexer_context *ctx, struct token_handler *params, struct bm_token *token)
{
	const char *cur = ctx->cur;
	const char *start;
	size_t len;

	start = cur;

	if (cur >= ctx->end || *cur != '#')
		return -1;

	while (++cur < ctx->end && *cur != '\n')
		;

	len = (size_t)(cur - start);

	lexer_token_init_str_dup(token, params->type, start, len);
	lexer_token_set_pos(ctx, token);

	ctx_upd(ctx, cur);
	return 0;
}

static int match_num(struct lexer_context *ctx, struct token_handler *params, struct bm_token *token)
{
	const char *cur = ctx->cur;
	char *int_end_ptr;
	int int_val;
	char *float_end_ptr;
	float float_val;

	U_MARK_USED(params);

	/* TODO: Write our own impl of strto* to allow non-\0-terminated strings */
	int_val = strtol(cur, &int_end_ptr, 10);
	float_val = strtof(cur, &float_end_ptr);

	if (int_end_ptr == cur && float_end_ptr == cur)
		return -1;

	if (int_end_ptr >= float_end_ptr)
	{
		lexer_token_init_int(token, BM_TOK_IMM_INT, int_val);
		cur = int_end_ptr;
	}
	else
	{
		lexer_token_init_float(token, BM_TOK_IMM_FLOAT, float_val);
		cur = float_end_ptr;
	}

	lexer_token_set_pos(ctx, token);

	ctx_upd(ctx, cur);
	return 0;
}

static int match_char(struct lexer_context *ctx, struct token_handler *params, struct bm_token *token)
{
	const char *cur = ctx->cur;
	size_t avail;
	char c;

	avail = (size_t)(ctx->end - cur);
	if (avail < 3)
		return -1;

	if (*cur != '\'')
		return -1;
	cur++;

	c = *cur;
	cur++;

	if (*cur != '\'')
		return -1;
	cur++;

	lexer_token_init_char(token, params->type, c);
	lexer_token_set_pos(ctx, token);

	ctx_upd(ctx, cur);
	return 0;
}

static int match_str_helper(char *dst, size_t *len, const char **cursor, const char *end)
{
	const char *cur = *cursor;
	int last_was_escape;
	size_t l = 0;
	int str_terminated = 0;

	if (cur >= end || *cur != '"')
		return -1;
	cur++;

	last_was_escape = 0;
	while (cur < end)
	{
		char c;

		c = *cur++;

		if (last_was_escape)
		{
			if (dst)
				*dst++ = c;
			l++;
			last_was_escape = 0;
			continue;
		}

		if (c == '\\')
		{
			last_was_escape = 1;
			continue;
		}

		if (c == '"')
		{
			str_terminated = 1;
			break;
		}

		if (dst)
			*dst++ = c;
		l++;
	}

	if (!str_terminated)
		return -1;

	if (dst)
	{
		*dst++ = '\0';
		*cursor = cur;
	}

	if (len)
		*len = l;

	return 0;
}

static int match_str(struct lexer_context *ctx, struct token_handler *params, struct bm_token *token)
{
	const char *cur = ctx->cur;
	int result;
	size_t len;
	char *dst;

	U_MARK_USED(params);

	result = match_str_helper(NULL, &len, &cur, ctx->end);
	if (result < 0)
		return -1;

	dst = malloc(len + 1);
	if (!dst)
		return -1;

	match_str_helper(dst, NULL, &cur, ctx->end);

	lexer_token_init_str(token, params->type, dst);
	lexer_token_set_pos(ctx, token);

	ctx_upd(ctx, cur);
	return 0;
}

static int match_identifier(struct lexer_context *ctx, struct token_handler *params, struct bm_token *token)
{
	const char *cur = ctx->cur;
	const char *start;
	size_t len;

	start = cur;

	if (cur > ctx->line && is_identifier_char(cur[-1]))
		return -1;

	if (!is_identifier_first_char(*cur))
		return -1;
	cur++;

	while (is_identifier_char(*cur))
		cur++;

	len = (size_t)(cur - start);

	lexer_token_init_str_dup(token, params->type, start, len);
	lexer_token_set_pos(ctx, token);

	ctx_upd(ctx, cur);
	return 0;
}

static int match_identifier_lalnu(struct lexer_context *ctx, struct token_handler *params, struct bm_token *token)
{
	const char *cur = ctx->cur;
	const char *start;
	size_t len;

	start = cur;

	if (cur > ctx->line && is_identifier_char(cur[-1]))
		return -1;

	if (!is_identifier_lalnum_first_char(*cur))
		return -1;
	cur++;

	while (is_identifier_lalnum_char(*cur))
		cur++;

	if (is_identifier_char(*cur))
		return -1;

	len = (size_t)(cur - start);

	lexer_token_init_str_dup(token, params->type, start, len);
	lexer_token_set_pos(ctx, token);

	ctx_upd(ctx, cur);
	return 0;
}

static int match_identifier_lsnak(struct lexer_context *ctx, struct token_handler *params, struct bm_token *token)
{
	const char *cur = ctx->cur;
	const char *start;
	size_t len;

	start = cur;

	if (cur > ctx->line && is_identifier_char(cur[-1]))
		return -1;

	if (!is_identifier_lsnake_first_char(*cur))
		return -1;
	cur++;

	while (is_identifier_lsnake_char(*cur))
		cur++;

	if (is_identifier_char(*cur))
		return -1;

	len = (size_t)(cur - start);

	lexer_token_init_str_dup(token, params->type, start, len);
	lexer_token_set_pos(ctx, token);

	ctx_upd(ctx, cur);
	return 0;
}

static int match_identifier_ucaml(struct lexer_context *ctx, struct token_handler *params, struct bm_token *token)
{
	const char *cur = ctx->cur;
	const char *start;
	size_t len;

	start = cur;

	if (cur > ctx->line && is_identifier_char(cur[-1]))
		return -1;

	if (!is_identifier_ucamel_first_char(*cur))
		return -1;
	cur++;

	while (is_identifier_ucamel_char(*cur))
		cur++;

	if (is_identifier_char(*cur))
		return -1;

	len = (size_t)(cur - start);

	lexer_token_init_str_dup(token, params->type, start, len);
	lexer_token_set_pos(ctx, token);

	ctx_upd(ctx, cur);
	return 0;
}

static struct token_handler token_handlers[] = {
#define H(precedence, id, str, cb) { (precedence), (id), (str), (U_STRLEN(str)), (cb) }
	H(0, BM_TOK_UNKNOWN,                   "",                NULL                  ),
	H(0, BM_TOK_ABSTRACT,                  "abstract",        match_isolated_keyword),
	H(0, BM_TOK_ALIAS,                     "alias",           match_isolated_keyword),
	H(1, BM_TOK_ANNOTATION,                "@",               match_simple_keyword  ),
	H(0, BM_TOK_ANNOTATION_AUTHORS,        "@authors",        match_isolated_keyword),
	H(0, BM_TOK_ANNOTATION_DEPRECATED,     "@deprecated",     match_isolated_keyword),
	H(0, BM_TOK_ANNOTATION_MAIN,           "@Main",           match_isolated_keyword),
	H(0, BM_TOK_ANNOTATION_READONLY,       "@readonly",       match_isolated_keyword),
	H(0, BM_TOK_ARROW,                     "->",              match_simple_keyword  ),
	H(0, BM_TOK_AS,                        "as",              match_isolated_keyword),
	H(0, BM_TOK_BRACKET_CLOSE,             ")",               match_simple_keyword  ),
	H(0, BM_TOK_BRACKET_OPEN,              "(",               match_simple_keyword  ),
	H(0, BM_TOK_BREAK,                     "break",           match_isolated_keyword),
	H(0, BM_TOK_CENUM,                     "cenum",           match_isolated_keyword),
	H(0, BM_TOK_CLASS,                     "class",           match_isolated_keyword),
	H(0, BM_TOK_COLON,                     ":",               match_simple_keyword  ),
	H(0, BM_TOK_COMMA,                     ",",               match_simple_keyword  ),
	H(0, BM_TOK_COMMENT,                   "",                match_comment         ),
	H(0, BM_TOK_CONST,                     "const",           match_isolated_keyword),
	H(0, BM_TOK_CONTINUE,                  "continue",        match_isolated_keyword),
	H(0, BM_TOK_CURLY_BRACKET_CLOSE,       "}",               match_simple_keyword  ),
	H(0, BM_TOK_CURLY_BRACKET_OPEN,        "{",               match_simple_keyword  ),
	H(0, BM_TOK_DO,                        "do",              match_isolated_keyword),
	H(1, BM_TOK_DOT,                       ".",               match_simple_keyword  ),
	H(0, BM_TOK_ELLIPSIS,                  "...",             match_simple_keyword  ),
	H(0, BM_TOK_ELSE,                      "else",            match_isolated_keyword),
	H(0, BM_TOK_ENUM,                      "enum",            match_isolated_keyword),
	H(0, BM_TOK_EOF,                       "",                NULL                  ),
	H(0, BM_TOK_EOL,                       "\n",              match_eol             ),
	H(0, BM_TOK_EXTENDS,                   "extends",         match_isolated_keyword),
	H(0, BM_TOK_FALSE,                     "false",           match_isolated_keyword),
	H(0, BM_TOK_FINAL,                     "final",           match_isolated_keyword),
	H(0, BM_TOK_FOR,                       "for",             match_isolated_keyword),
	H(0, BM_TOK_IF,                        "if",              match_isolated_keyword),
	H(6, BM_TOK_IDENTIFIER,                "",                match_identifier      ),
	H(4, BM_TOK_IDENTIFIER_LALNUM,         "",                match_identifier_lalnu),
	H(5, BM_TOK_IDENTIFIER_LSNAKE,         "",                match_identifier_lsnak),
	H(4, BM_TOK_IDENTIFIER_UCAMEL,         "",                match_identifier_ucaml),
	H(0, BM_TOK_IMM_CHAR,                  "",                match_char            ),
	H(0, BM_TOK_IMM_FLOAT,                 "",                match_num             ),
	H(0, BM_TOK_IMM_INT,                   "",                NULL /* match_num */  ),
	H(0, BM_TOK_IMM_STR,                   "",                match_str             ),
	H(0, BM_TOK_IMPLEMENTS,                "implements",      match_isolated_keyword),
	H(0, BM_TOK_IMPORT,                    "import",          match_isolated_keyword),
	H(0, BM_TOK_INTERFACE,                 "interface",       match_isolated_keyword),
	H(0, BM_TOK_LOOP,                      "loop",            match_isolated_keyword),
	H(0, BM_TOK_MATCH,                     "match",           match_isolated_keyword),
	H(0, BM_TOK_NEW,                       "new",             match_isolated_keyword),
	H(0, BM_TOK_NULL,                      "null",            match_isolated_keyword),
	H(1, BM_TOK_OP_ADD,                    "+",               match_simple_keyword  ),
	H(0, BM_TOK_OP_AND,                    "&&",              match_isolated_keyword),
	H(2, BM_TOK_OP_ASSIGN,                 "=",               match_simple_keyword  ),
	H(0, BM_TOK_OP_ASSIGN_ADD,             "+=",              match_simple_keyword  ),
	H(0, BM_TOK_OP_ASSIGN_BITWISE_AND,     "&=",              match_simple_keyword  ),
	H(0, BM_TOK_OP_ASSIGN_BITWISE_OR,      "|=",              match_simple_keyword  ),
	H(0, BM_TOK_OP_ASSIGN_BITWISE_XOR,     "^=",              match_simple_keyword  ),
	H(0, BM_TOK_OP_ASSIGN_DIV,             "/=",              match_simple_keyword  ),
	H(2, BM_TOK_OP_ASSIGN_MUL,             "*=",              match_simple_keyword  ),
	H(0, BM_TOK_OP_ASSIGN_MOD,             "%=",              match_simple_keyword  ),
	H(0, BM_TOK_OP_ASSIGN_POW,             "**=",             match_simple_keyword  ),
	H(0, BM_TOK_OP_ASSIGN_SHIFT_LEFT,      "<<=",             match_simple_keyword  ),
	H(0, BM_TOK_OP_ASSIGN_SHIFT_RIGHT,     ">>=",             match_simple_keyword  ),
	H(0, BM_TOK_OP_ASSIGN_SUB,             "-=",              match_simple_keyword  ),
	H(1, BM_TOK_OP_BITWISE_AND,            "&",               match_simple_keyword  ),
	H(0, BM_TOK_OP_BITWISE_NOT,            "~",               match_simple_keyword  ),
	H(1, BM_TOK_OP_BITWISE_OR,             "|",               match_simple_keyword  ),
	H(1, BM_TOK_OP_BITWISE_XOR,            "^",               match_simple_keyword  ),
	H(0, BM_TOK_OP_DEC,                    "--",              match_simple_keyword  ),
	H(1, BM_TOK_OP_DIV,                    "/",               match_simple_keyword  ),
	H(0, BM_TOK_OP_EQUAL,                  "==",              match_simple_keyword  ),
	H(0, BM_TOK_OP_GEQ,                    ">=",              match_simple_keyword  ),
	H(1, BM_TOK_OP_GREATER,                ">",               match_simple_keyword  ),
	H(0, BM_TOK_OP_INC,                    "++",              match_simple_keyword  ),
	H(0, BM_TOK_OP_LEQ,                    "<=",              match_simple_keyword  ),
	H(1, BM_TOK_OP_LOWER,                  "<",               match_simple_keyword  ),
	H(1, BM_TOK_OP_MOD,                    "%",               match_simple_keyword  ),
	H(3, BM_TOK_OP_MUL,                    "*",               match_simple_keyword  ),
	H(0, BM_TOK_OP_NEQ,                    "!=",              match_simple_keyword  ),
	H(0, BM_TOK_OP_NOT,                    "!",               match_isolated_keyword),
	H(0, BM_TOK_OP_OR,                     "||",              match_isolated_keyword),
	H(1, BM_TOK_OP_POW,                    "**",              match_simple_keyword  ),
	H(0, BM_TOK_OP_SHIFT_LEFT,             "<<",              match_simple_keyword  ),
	H(0, BM_TOK_OP_SHIFT_RIGHT,            ">>",              match_simple_keyword  ),
	H(0, BM_TOK_OP_TO_NONNULLABLE,         "??",              match_simple_keyword  ),
	H(1, BM_TOK_OP_SUB,                    "-",               match_simple_keyword  ),
	H(0, BM_TOK_OP_XOR,                    "xor",             match_isolated_keyword),
	H(0, BM_TOK_PACK,                      "pack",            match_isolated_keyword),
	H(0, BM_TOK_PARENT,                    "parent",          match_isolated_keyword),
	H(0, BM_TOK_PRIVATE,                   "private",         match_isolated_keyword),
	H(0, BM_TOK_PROTECTED,                 "protected",       match_isolated_keyword),
	H(0, BM_TOK_PUBLIC,                    "public",          match_isolated_keyword),
	H(0, BM_TOK_QUESTION_MARK,             "?",               match_simple_keyword  ),
	H(0, BM_TOK_RETURN,                    "return",          match_isolated_keyword),
	H(0, BM_TOK_SEMICOLON,                 ";",               match_simple_keyword  ),
	H(0, BM_TOK_SQUARE_BRACKET_CLOSE,      "]",               match_simple_keyword  ),
	H(0, BM_TOK_SQUARE_BRACKET_OPEN,       "[",               match_simple_keyword  ),
	H(0, BM_TOK_STATIC,                    "static",          match_isolated_keyword),
	H(0, BM_TOK_THIS,                      "this",            match_isolated_keyword),
	H(0, BM_TOK_TILL,                      "till",            match_isolated_keyword),
	H(0, BM_TOK_TO,                        "to",              match_isolated_keyword),
	H(0, BM_TOK_TRUE,                      "true",            match_isolated_keyword),
	H(0, BM_TOK_USE,                       "use",             match_isolated_keyword),
	H(0, BM_TOK_VAR,                       "var",             match_isolated_keyword),
	H(0, BM_TOK_WHILE,                     "while",           match_isolated_keyword)
#undef H
};

static int match_token(struct lexer_context *ctx, struct bm_token *token)
{
	for (size_t i = 0; i < U_ARRAY_SIZE(token_handlers); i++)
	{
		struct token_handler *handler = &token_handlers[i];

		if (!handler->callback)
			continue;

		if (handler->callback(ctx, handler, token) == 0)
			return 0;
	}

	return -1;
}

void lexer(struct lexer_context *ctx, const void *in, size_t in_size)
{
	ctx->start = in;
	ctx->cur = in;
	ctx->line = in;
	ctx->end = &ctx->start[in_size];
	ctx->off = 0;
	ctx->lineno = 1;
	ctx->colno = 1;
}

void lexer_backup(struct lexer_context *ctx, struct lexer_context *b)
{
	*b = *ctx;
}

void lexer_restore(struct lexer_context *ctx, struct lexer_context *b)
{
	*ctx = *b;
}

int lexer_next(struct lexer_context *ctx, struct bm_token *token)
{
	int result;

	/* We already reached EOF */
	if (!ctx->cur)
		return -1;

	skip_spaces(ctx);

	/* We just reached EOF */
	if (ctx->cur == ctx->end)
	{
		lexer_token_init(token, BM_TOK_EOF);
		ctx->cur = NULL;
		return 0;
	}

	result = match_token(ctx, token);
	if (result < 0)
		return -1;
	return 0;
}

static int compare_tok_handler_precedence(const void *p1, const void *p2)
{
	const struct token_handler *h1 = p1;
	const struct token_handler *h2 = p2;

	/* Move callback-less handlers to the end of the array */
	if (!h1->callback != !h2->callback)
		return !h1->callback - !h2->callback;

	return h1->precedence - h2->precedence;
}

void lexer_init(void)
{
	qsort(token_handlers, U_ARRAY_SIZE(token_handlers), sizeof(token_handlers[0]), compare_tok_handler_precedence);
}
