/*
 * Copyright (C) 2017-2020 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LEXER_H
#define LEXER_H 1

#include "token.h"

struct lexer_context
{
	const char *start;
	const char *cur;
	const char *line;
	const char *end;
	/* Bytes */
	size_t off;
	/* UTF-8 chars */
	size_t lineno;
	size_t colno;
};

extern int lexer_token_deinit(struct bm_token *token);
extern void lexer(struct lexer_context *ctx, const void *in, size_t in_size);
extern void lexer_backup(struct lexer_context *ctx, struct lexer_context *b);
extern void lexer_restore(struct lexer_context *ctx, struct lexer_context *b);
extern int lexer_next(struct lexer_context *ctx, struct bm_token *token);
extern void lexer_init(void);

#endif
