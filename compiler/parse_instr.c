/*
 * Copyright (C) 2017-2020 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "util/macro.h"
#include "ast.h"
#include "parser.h"

#include "parser_priv.h"
#include "parse_expr.h"
#include "parse_instr.h"

typedef int (*parse_instr_callback)(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out);


static int parse_instr_break(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	unsigned long i = *cur_tok_id;
	struct bm_ast_node nbreak;

	if (ctx->in->tokens[i].type != BM_TOK_BREAK)
		return -1;
	i++;

	parser_node_init(&nbreak, BM_AST_NODE_INSTR_BREAK);

	*out = nbreak;

	*cur_tok_id = i;
	return 0;
}

static int parse_instr_continue(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	unsigned long i = *cur_tok_id;
	struct bm_ast_node ncontinue;

	if (ctx->in->tokens[i].type != BM_TOK_CONTINUE)
		return -1;
	i++;

	parser_node_init(&ncontinue, BM_AST_NODE_INSTR_CONTINUE);

	*out = ncontinue;

	*cur_tok_id = i;
	return 0;
}

static int instr_decl_dtor(struct bm_ast_node *node)
{
	return destroy_decl(&node->data.as_instr_decl.base);
}

static int instr_decl_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	const struct bm_ast_node_data_instr_decl *data = &node->data.as_instr_decl;

	debug_decl(&data->base, indent_lvl);

	return 0;
}

/* Handles decl_assign too. */
static int parse_instr_decl(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node decl;

	parser_node_init(&decl, BM_AST_NODE_INSTR_DECL);
	decl.dtor = instr_decl_dtor;
	decl.debug_data = instr_decl_debug_data;

	result = parse_decl(ctx, &i, &decl.data.as_instr_decl.base);
	if (result < 0)
		goto failed;

	if (ctx->in->tokens[i].type == BM_TOK_OP_ASSIGN)
	{
		struct bm_ast_node expr;

		decl.type = BM_AST_NODE_INSTR_DECL_ASSIGN;
		i++;

		result = parse_expr(ctx, &i, &expr);
		if (result < 0)
			goto failed;

		parser_node_set_child(&decl, BM_AST_NODE_INSTR_DECL_ASSIGN_CHILD_EXPR, &expr);
	}

	*out = decl;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&decl);
	return -1;
}

int parser_node_init_instr_noop(struct bm_ast_node *node)
{
	return parser_node_init(node, BM_AST_NODE_INSTR_NOOP);
}

static int parse_instr_ret(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node expr;
	struct bm_ast_node ret;

	if (ctx->in->tokens[i].type != BM_TOK_RETURN)
		return -1;
	i++;

	parser_node_init(&ret, BM_AST_NODE_INSTR_RET);

	result = parse_expr(ctx, &i, &expr);
	if (result == 0)
		parser_node_set_child(&ret, BM_AST_NODE_INSTR_RET_CHILD_EXPR, &expr);

	*out = ret;

	*cur_tok_id = i;
	return 0;
}

static const parse_instr_callback instr_callback_table[] = {
	parse_instr_break,
	parse_instr_continue,
	parse_instr_decl,
	parse_instr_ret,
	parse_expr
};

int parse_instr(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	int result;

	for (unsigned int i = 0; i < ARRAY_SIZE(instr_callback_table); i++)
	{
		parse_instr_callback callback = instr_callback_table[i];

		result = callback(ctx, cur_tok_id, out);
		if (result == 0)
			return 0;
	}

	return -1;
}
