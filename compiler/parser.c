/*
 * Copyright (C) 2017-2020 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util/ascii.h"
#include "util/macro.h"
#include "util/misc.h"
#include "ast.h"
#include "debug.h"
#include "token.h"
#include "lexer.h"
#include "utils.h"
#include "parser.h"

#include "parser_priv.h"
#include "parse_expr.h"
#include "parse_instr.h"


static inline enum bm_ast_visibility parser_visibility_from_token_type(enum bm_token_type type)
{
	switch (type)
	{
		case BM_TOK_PUBLIC:    return BM_AST_VISIBILITY_PUBLIC;
		case BM_TOK_PRIVATE:   return BM_AST_VISIBILITY_PRIVATE;
		case BM_TOK_PROTECTED: return BM_AST_VISIBILITY_PROTECTED;
		default:               return BM_AST_VISIBILITY_INVAL;
	}
}

static int parser_node_debug(const struct bm_ast_node *node, int indent_lvl)
{
	printf("{\n");
	indent_lvl++;

	printf("%*s\"type\": \"%s\"", indent_lvl * 2, "", debug_node_type(node->type));

	if (node->debug_data)
		node->debug_data(node, indent_lvl);

	if (node->children_count)
		printf(",\n%*s\"children\": [", indent_lvl * 2, "");

	for (unsigned long i = 0; i < node->children_count; i++)
	{
		parser_node_debug(&node->children[i], indent_lvl);

		if (i != node->children_count - 1)
			printf(", ");
	}

	if (node->children_count)
		printf("]");

	indent_lvl--;
	printf("\n%*s}", indent_lvl * 2, "");

	return 0;
}

static int parser_node_dummy_dtor(struct bm_ast_node *node)
{
	MARK_USED(node);

	return 0;
}

static int parser_node_dummy_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	MARK_USED(node);
	MARK_USED(indent_lvl);

	return 0;
}

int parser_node_init(struct bm_ast_node *node, enum bm_ast_node_type type)
{
	node->type = type;
	memset(&node->data, 0, sizeof(node->data));
	node->children_count = 0;
	node->children = NULL;
	node->dtor = parser_node_dummy_dtor;
	node->debug_data = parser_node_dummy_debug_data;
	return 0;
}

unsigned long parser_node_get_children_count(struct bm_ast_node *node)
{
	return node->children_count;
}

struct bm_ast_node *parser_node_get_child(struct bm_ast_node *node, unsigned long id)
{
	if (id >= node->children_count)
		return NULL;

	return &node->children[id];
}

int parser_node_set_child(struct bm_ast_node *node, unsigned long id, struct bm_ast_node *child)
{
	struct bm_ast_node *children;

	if (id >= node->children_count)
	{
		unsigned long new_children_count = id + 1;

		children = realloc(node->children, new_children_count * sizeof(node->children[0]));
		if (!children)
			return -1;

		/* We do not need to initialize the last node as we overwrite it later. */
		for (unsigned long i = node->children_count; i < new_children_count - 1; i++)
			parser_node_init_unknown(&children[i]);

		node->children_count = new_children_count;
		node->children = children;
	}

	node->children[id] = *child;
	return 0;
}

int parser_node_append_child(struct bm_ast_node *node, struct bm_ast_node *child)
{
	return parser_node_set_child(node, node->children_count, child);
}

struct bm_ast_node *parser_node_exchange_with_child(struct bm_ast_node *node, unsigned long child_id, unsigned long child_sub_node_id)
{
	struct bm_ast_node old_parent_node;
	struct bm_ast_node old_child_node;
	struct bm_ast_node *old_child_sub_node;

	old_parent_node = *node;
	old_child_node = *parser_node_get_child(&old_parent_node, child_id);
	old_child_sub_node = parser_node_get_child(&old_child_node, child_sub_node_id);

	parser_node_set_child(&old_parent_node, child_id, old_child_sub_node);
	parser_node_set_child(&old_child_node, child_sub_node_id, &old_parent_node);

	*node = old_child_node;

	return parser_node_get_child(node, child_sub_node_id);
}

static int parser_node_destroy_no_recurse(struct bm_ast_node *node)
{
	if (node->children)
		free(node->children);
	node->children = NULL;

	if (node->dtor)
		node->dtor(node);

	parser_node_init_unknown(node);

	return 0;
}

int parser_node_destroy(struct bm_ast_node *node)
{
	for (unsigned long i = 0; i < node->children_count; i++)
		parser_node_destroy(&node->children[i]);

	return parser_node_destroy_no_recurse(node);
}

int parser_ignore_eols_or_semicolons(struct parser_ctx *ctx, unsigned long *cur_tok_id)
{
	unsigned long i = *cur_tok_id;

	while (i < ctx->in->nb_tokens)
	{
		struct bm_token *token = &ctx->in->tokens[i];

		if (token->type != BM_TOK_EOL
				&& token->type != BM_TOK_SEMICOLON)
			break;

		i++;
	}

	*cur_tok_id = i;
	return 0;
}

int parser_ignore_eols(struct parser_ctx *ctx, unsigned long *cur_tok_id)
{
	unsigned long i = *cur_tok_id;

	while (i < ctx->in->nb_tokens)
	{
		struct bm_token *token = &ctx->in->tokens[i];

		if (token->type != BM_TOK_EOL)
			break;

		i++;
	}

	*cur_tok_id = i;
	return 0;
}

static int parse_package_name(const char *name)
{
	if (!u_ascii_islower(*name) && *name != '_')
		return -1;

	name++;
	while (*name)
	{
		if (!u_ascii_islower(*name) && !u_ascii_isdigit(*name) && *name != '_')
			return -1;

		name++;
	}

	return 0;
}

int parse_class_name(const char *name)
{
	if (!u_ascii_isupper(*name))
		return -1;

	name++;
	while (*name)
	{
		if (!u_ascii_isalnum(*name))
			return -1;

		name++;
	}

	return 0;
}

static int parse_func_name(const char *name)
{
	if (!u_ascii_islower(*name))
		return -1;

	name++;
	while (*name)
	{
		if (!u_ascii_isalnum(*name) && *name != '_')
			return -1;

		name++;
	}

	return 0;
}

int parse_interface_name(const char *name)
{
	return parse_class_name(name);
}

int parse_var_name(const char *name)
{
	return parse_func_name(name);
}

static int create_empty_class_path(struct bm_ast_class_path *out)
{
	buffer_init_hint(&out->seqs, 10);
	return 0;
}

int destroy_class_path(struct bm_ast_class_path *class_path)
{
	buffer_free(&class_path->seqs);
	return 0;
}

int debug_class_path(const struct bm_ast_class_path *class_path)
{
	for (size_t i = 0; i < class_path->seqs.nb_lines; i++)
		printf("%s%s", i > 0 ? "." : "", buffer_get_line(&class_path->seqs, i));

	return 0;
}

static int class_path_append_seq(struct bm_ast_class_path *class_path, const char *seq)
{
	buffer_append_line(&class_path->seqs, seq);
	return 0;
}

int parse_class_path(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_class_path *out)
{
	unsigned long i = *cur_tok_id;
	int last_was_id = -1;
	int result;

	create_empty_class_path(out);

	while (i < ctx->in->nb_tokens)
	{
		struct bm_token *token = &ctx->in->tokens[i];
		int leave = 0;

		switch (token->type)
		{
			case BM_TOK_EOL:
			case BM_TOK_SEMICOLON:
				leave = 1;
				break;

			case BM_TOK_IDENTIFIER:
				if (last_was_id > 0)
				{
					leave = 1;
					break;
				}

				result = parse_package_name(token->data.as_str);
				if (result < 0)
				{
					leave = 1;
					break;
				}

				buffer_append_line(&out->seqs, token->data.as_str);

				last_was_id = 1;
				break;

			case BM_TOK_DOT:
				if (!last_was_id)
				{
					leave = 1;
					break;
				}

				last_was_id = 0;
				break;

			default:
				leave = 1;
		}

		if (leave)
			break;

		i++;
	}

	if (last_was_id < 0)
	{
		destroy_class_path(out);
		return -1;
	}

	if (!last_was_id)
		i--;

	*cur_tok_id = i;
	return 0;
}

int clone_class_path(struct bm_ast_class_path *class_path, struct bm_ast_class_path *out)
{
	return buffer_clone(&class_path->seqs, &out->seqs);
}

int compare_class_path(struct bm_ast_class_path *a, struct bm_ast_class_path *b)
{
	size_t a_len = buffer_get_line_count(&a->seqs);
	size_t b_len = buffer_get_line_count(&b->seqs);

	if (a_len != b_len)
		return a_len - b_len;

	for (size_t i = 0; i < a_len; i++)
	{
		int result;
		const char *a_str = buffer_get_line(&a->seqs, i);
		const char *b_str = buffer_get_line(&b->seqs, i);

		result = strcmp(a_str, b_str);
		if (result != 0)
			return result;
	}

	return 0;
}

int destroy_type(struct bm_ast_type *type)
{
	destroy_class_path(&type->class_path);

	if (type->class_name)
		free(type->class_name);

	while (type->nb_generic_params > 0)
	{
		destroy_type(&type->generic_params[type->nb_generic_params - 1]);
		type->nb_generic_params--;
	}

	if (type->generic_params)
		free(type->generic_params);

	return 0;
}

int debug_type(const struct bm_ast_type *type)
{
	int result;

	result = debug_class_path(&type->class_path);
	if (result == 0)
		printf(".");

	printf("%s", type->class_name);

	if (type->nb_generic_params)
		printf("<");

	for (unsigned long i = 0; i < type->nb_generic_params; i++)
	{
		debug_type(&type->generic_params[i]);

		if (i != type->nb_generic_params - 1)
			printf(", ");
	}

	if (type->nb_generic_params)
		printf(">");

	return 0;
}

static int parse_tuple_type(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_type *out)
{
	int result;
	unsigned long i = *cur_tok_id;

	create_empty_class_path(&out->class_path);
	out->class_name = NULL;
	out->nb_generic_params = 0;
	out->generic_params = NULL;

	if (ctx->in->tokens[i].type != BM_TOK_BRACKET_OPEN)
		goto failed;
	i++;

	class_path_append_seq(&out->class_path, "bm");
	class_path_append_seq(&out->class_path, "lang");

	out->class_name = u_strdup("Tuple");
	if (!out->class_name)
		goto failed;

	while (i < ctx->in->nb_tokens)
	{
		struct bm_ast_type *params;

		params = realloc(out->generic_params, (out->nb_generic_params + 1) * sizeof(out->generic_params[0]));
		if (!params)
			goto failed;
		out->generic_params = params;

		result = parse_type(ctx, &i, &out->generic_params[out->nb_generic_params]);
		if (result < 0)
			break;
		out->nb_generic_params++;

		if (ctx->in->tokens[i].type != BM_TOK_COMMA)
			break;
		i++;
	}

	/* Tuple needs at least 2 generic params. */
	if (out->nb_generic_params < 2)
		goto failed;

	if (ctx->in->tokens[i].type != BM_TOK_BRACKET_CLOSE)
		goto failed;
	i++;

	*cur_tok_id = i;
	return 0;

failed:
	destroy_type(out);
	return -1;
}

static int parse_func_type(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_type *out)
{
	int result;
	unsigned long i = *cur_tok_id;

	create_empty_class_path(&out->class_path);
	out->class_name = NULL;
	out->nb_generic_params = 0;
	out->generic_params = NULL;

	if (ctx->in->tokens[i].type != BM_TOK_BRACKET_OPEN)
		goto failed;
	i++;

	class_path_append_seq(&out->class_path, "bm");
	class_path_append_seq(&out->class_path, "lang");

	out->class_name = u_strdup("Func");
	if (!out->class_name)
		goto failed;

	while (i < ctx->in->nb_tokens)
	{
		struct bm_ast_type *params;

		/* Allocate 2 elements because Func needs at least 2 generic params. */
		params = realloc(out->generic_params, (out->nb_generic_params + 2) * sizeof(out->generic_params[0]));
		if (!params)
			goto failed;
		out->generic_params = params;

		result = parse_type(ctx, &i, &out->generic_params[out->nb_generic_params]);
		if (result < 0)
			break;
		out->nb_generic_params++;

		if (ctx->in->tokens[i].type != BM_TOK_COMMA)
			break;
		i++;
	}

	if (out->nb_generic_params < 1)
		goto failed;

	if (ctx->in->tokens[i].type != BM_TOK_ARROW)
		goto failed;
	i++;

	/* The storage for the last param was allocated in the loop. */
	result = parse_type(ctx, &i, &out->generic_params[out->nb_generic_params]);
	if (result < 0)
		goto failed;
	out->nb_generic_params++;

	if (ctx->in->tokens[i].type != BM_TOK_BRACKET_CLOSE)
		goto failed;
	i++;

	*cur_tok_id = i;
	return 0;

failed:
	destroy_type(out);
	return -1;
}

int parse_type(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_type *out)
{
	int result;
	unsigned long i = *cur_tok_id;

	result = parse_func_type(ctx, cur_tok_id, out);
	if (result == 0)
		return 0;

	result = parse_tuple_type(ctx, cur_tok_id, out);
	if (result == 0)
		return 0;

	out->class_name = NULL;
	out->nb_generic_params = 0;
	out->generic_params = NULL;

	result = parse_class_path(ctx, &i, &out->class_path);
	if (result == 0)
	{
		if (ctx->in->tokens[i].type != BM_TOK_DOT)
			goto failed;
		i++;
	}
	else
		create_empty_class_path(&out->class_path);

	if (ctx->in->tokens[i].type != BM_TOK_IDENTIFIER)
		goto failed;

	result = parse_class_name(ctx->in->tokens[i].data.as_str);
	if (result < 0)
		goto failed;

	out->class_name = u_strdup(ctx->in->tokens[i].data.as_str);
	if (!out->class_name)
		goto failed;
	i++;

	if (ctx->in->tokens[i].type == BM_TOK_OP_LOWER)
	{
		i++;

		while (i < ctx->in->nb_tokens)
		{
			struct bm_ast_type *params;

			params = realloc(out->generic_params, (out->nb_generic_params + 1) * sizeof(out->generic_params[0]));
			if (!params)
				goto failed;
			out->generic_params = params;

			result = parse_type(ctx, &i, &out->generic_params[out->nb_generic_params]);
			if (result < 0)
				break;
			out->nb_generic_params++;

			if (ctx->in->tokens[i].type != BM_TOK_COMMA)
				break;
			i++;
		}

		if (ctx->in->tokens[i].type == BM_TOK_OP_GREATER)
		{
			i++;
		}
		else if (ctx->in->tokens[i].type == BM_TOK_OP_SHIFT_RIGHT)
		{
			if (ctx->last_rshift_was_greater)
				i++;

			ctx->last_rshift_was_greater = !ctx->last_rshift_was_greater;
		}
		else
		{
			goto failed;
		}
	}

	*cur_tok_id = i;
	return 0;

failed:
	destroy_type(out);
	return -1;
}

int clone_type(struct bm_ast_type *type, struct bm_ast_type *out)
{
	int result;

	/* Initialize those so that destroy_type() will work properly. */
	out->class_name = NULL;
	out->nb_generic_params = 0;
	out->generic_params = NULL;

	result = clone_class_path(&type->class_path, &out->class_path);
	if (result < 0)
		return -1;

	out->class_name = u_strdup(type->class_name);
	if (!out->class_name)
		goto failed;

	out->generic_params = calloc(type->nb_generic_params, sizeof(type->generic_params[0]));
	if (!out->generic_params)
		goto failed;

	for (unsigned long i = 0; i < type->nb_generic_params; i++)
	{
		result = clone_type(&type->generic_params[i], &out->generic_params[i]);
		if (result < 0)
			goto failed;

		out->nb_generic_params++;
	}

	return 0;

failed:
	destroy_type(out);
	return -1;
}

int parser_node_init_unknown(struct bm_ast_node *node)
{
	return parser_node_init(node, BM_AST_NODE_UNKNOWN);
}

int destroy_decl(struct bm_ast_decl *decl)
{
	destroy_type(&decl->type);

	if (decl->name)
		free(decl->name);
	decl->name = NULL;

	return 0;
}

int debug_decl(const struct bm_ast_decl *decl, int indent_lvl)
{
	printf(",\n%*s\"name\": \"%s\"", indent_lvl * 2, "", decl->name);
	printf(",\n%*s\"decl_type\": \"", indent_lvl * 2, "");
	debug_type(&decl->type);
	printf("\"");

	return 0;
}

int parse_decl(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_decl *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_decl decl;

	decl.name = NULL;

	result = parse_type(ctx, &i, &decl.type);
	if (result < 0)
		return -1;

	if (ctx->in->tokens[i].type != BM_TOK_IDENTIFIER)
		goto failed;

	result = parse_var_name(ctx->in->tokens[i].data.as_str);
	if (result < 0)
		goto failed;

	decl.name = u_strdup(ctx->in->tokens[i].data.as_str);
	if (!decl.name)
		goto failed;
	i++;

	*out = decl;

	*cur_tok_id = i;
	return 0;

failed:
	destroy_decl(&decl);
	return -1;
}

static int destroy_decl_func(struct bm_ast_decl_func *decl)
{
	destroy_decl(&decl->base);
	buffer_free(&decl->param_names);

	return 0;
}

static int debug_decl_func(const struct bm_ast_decl_func *decl, int indent_lvl)
{
	const char *spc = "";

	debug_decl(&decl->base, indent_lvl);

	printf(",\n%*s\"params\": [", indent_lvl * 2, "");

	for (size_t i = 0; i < buffer_get_line_count(&decl->param_names); i++)
	{
		const char *param;
		size_t param_len;

		param = buffer_get_line(&decl->param_names, i);

		param_len = strlen(param);
		if (i == 0 && param_len == 0)
			break;

		if (i > 0)
			printf(",");

		printf(" \"%s\"", param);

		spc = " ";
	}

	printf("%s]", spc);

	return 0;
}

/* TODO: Handle variadic functions. */
static int parse_decl_func(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_decl_func *out, struct bm_ast_type *class_type)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_type ret_type;
	struct bm_ast_decl_func decl;

	decl.base.name = NULL;
	buffer_init_hint(&decl.param_names, 10);

	if (class_type && ctx->in->tokens[i].type == BM_TOK_NEW)
	{
		create_empty_class_path(&ret_type.class_path);
		ret_type.class_name = u_strdup("Void");
		ret_type.nb_generic_params = 0;
		ret_type.generic_params = NULL;

		if (!ret_type.class_name)
			goto failed_ret_type;

		decl.base.name = u_strdup("new");
		if (!decl.base.name)
			goto failed;
		i++;
	}
	else
	{
		result = parse_type(ctx, &i, &ret_type);
		if (result < 0)
			goto failed_ret_type;

		if (ctx->in->tokens[i].type != BM_TOK_IDENTIFIER)
			goto failed;

		result = parse_func_name(ctx->in->tokens[i].data.as_str);
		if (result < 0)
			goto failed;

		decl.base.name = u_strdup(ctx->in->tokens[i].data.as_str);
		if (!decl.base.name)
			goto failed;
		i++;
	}

	create_empty_class_path(&decl.base.type.class_path);
	decl.base.type.class_name = NULL;
	decl.base.type.nb_generic_params = 0;
	decl.base.type.generic_params = NULL;

	if (ctx->in->tokens[i].type != BM_TOK_BRACKET_OPEN)
		goto failed;
	i++;

	class_path_append_seq(&decl.base.type.class_path, "bm");
	class_path_append_seq(&decl.base.type.class_path, "lang");

	decl.base.type.class_name = u_strdup("Func");
	if (!decl.base.type.class_name)
		goto failed;

	if (class_type)
	{
		buffer_append_line(&decl.param_names, "this");

		decl.base.type.nb_generic_params++;
	}

	while (i < ctx->in->nb_tokens)
	{
		struct bm_ast_type *params;

		/* Allocate 2 elements because Func needs at least 2 generic params. */
		params = realloc(decl.base.type.generic_params, (decl.base.type.nb_generic_params + 2) * sizeof(decl.base.type.generic_params[0]));
		if (!params)
			goto failed;
		decl.base.type.generic_params = params;

		result = parse_type(ctx, &i, &decl.base.type.generic_params[decl.base.type.nb_generic_params]);
		if (result < 0)
		{
			/* The function does not have any param. */
			if (decl.base.type.nb_generic_params < (class_type ? 2 : 1))
				break;

			clone_type(&decl.base.type.generic_params[decl.base.type.nb_generic_params - 1], &decl.base.type.generic_params[decl.base.type.nb_generic_params]);
		}
		decl.base.type.nb_generic_params++;

		if (ctx->in->tokens[i].type != BM_TOK_IDENTIFIER)
			goto failed;

		buffer_append_line(&decl.param_names, ctx->in->tokens[i].data.as_str);
		i++;

		if (ctx->in->tokens[i].type != BM_TOK_COMMA)
			break;
		i++;
	}

	/* The storage for the class param was allocated in the loop. */
	if (class_type)
		clone_type(class_type, &decl.base.type.generic_params[0]);

	/* The storage for the first param was allocated in the loop. */
	if (decl.base.type.nb_generic_params < 1)
	{
		/* If no arguments were specified, add an argument of type Void. */
		decl.base.type.generic_params[0].class_name = u_strdup("Void");
		create_empty_class_path(&decl.base.type.generic_params[0].class_path);
		decl.base.type.generic_params[0].nb_generic_params = 0;
		decl.base.type.generic_params[0].generic_params = NULL;
		decl.base.type.nb_generic_params++;

		buffer_append_line(&decl.param_names, "");
	}

	if (ctx->in->tokens[i].type != BM_TOK_BRACKET_CLOSE)
		goto failed;
	i++;

	/* The storage for the last param was allocated in the loop. */
	decl.base.type.generic_params[decl.base.type.nb_generic_params] = ret_type;
	decl.base.type.nb_generic_params++;

	*out = decl;

	*cur_tok_id = i;
	return 0;

failed:
	destroy_decl_func(&decl);
	destroy_type(&ret_type);

failed_ret_type:
	return -1;
}

/* Parse nodes. */

static int pack_dtor(struct bm_ast_node *node)
{
	destroy_class_path(&node->data.as_pack.path);
	return 0;
}

static int pack_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	const struct bm_ast_node_data_pack *data = &node->data.as_pack;

	printf(",\n%*s\"path\": \"", indent_lvl * 2, "");
	debug_class_path(&data->path);
	printf("\"");

	return 0;
}

static int parse_pack(struct parser_ctx *ctx, unsigned long *cur_tok_id)
{
	unsigned long i = *cur_tok_id;
	struct bm_ast_class_path path;
	int ret;

	if (ctx->in->tokens[i].type != BM_TOK_PACK)
		return -1;
	i++;

	ret = parse_class_path(ctx, &i, &path);
	if (ret < 0)
	{
		printf("No valid path found in pack directive.\n");
		return -1;
	}

	if (ctx->in->tokens[i].type != BM_TOK_EOL
			&& ctx->in->tokens[i].type != BM_TOK_SEMICOLON)
	{
		printf("Expected newline or ';' at the end of the pack directive, got %s.\n", debug_token_type(ctx->in->tokens[i].type));
		goto failed;
	}
	i++;

	parser_node_init(&ctx->ast.package, BM_AST_NODE_PACK);
	ctx->ast.package.dtor = pack_dtor;
	ctx->ast.package.debug_data = pack_debug_data;

	ctx->ast.package.data.as_pack.path = path;

	*cur_tok_id = i;
	return 0;

failed:
	destroy_class_path(&path);
	return -1;
}

static int use_dtor(struct bm_ast_node *node)
{
	destroy_class_path(&node->data.as_use.path);

	if (node->data.as_use.name)
		free(node->data.as_use.name);
	node->data.as_use.name = NULL;

	if (node->data.as_use.alias_name)
		free(node->data.as_use.alias_name);
	node->data.as_use.alias_name = NULL;

	return 0;
}

static int use_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	const struct bm_ast_node_data_use *data = &node->data.as_use;

	printf(",\n%*s\"path\": \"", indent_lvl * 2, "");
	debug_class_path(&data->path);
	printf("\"");

	if (data->name)
		printf(",\n%*s\"name\": \"%s\"", indent_lvl * 2, "", data->name);

	if (data->alias_name)
		printf(",\n%*s\"alias\": \"%s\"", indent_lvl * 2, "", data->alias_name);

	return 0;
}

static int parse_use(struct parser_ctx *ctx, unsigned long *cur_tok_id)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_class_path class_path;
	struct bm_ast_node uses;
	struct bm_ast_node use;

	if (ctx->in->tokens[i].type != BM_TOK_USE)
		return -1;
	i++;

	parser_node_init(&uses, BM_AST_NODE_UNKNOWN);

	result = parse_class_path(ctx, &i, &class_path);
	if (result < 0)
	{
		printf("Expected a classpath inside use directive, got %s.\n", debug_token_type(ctx->in->tokens[i].type));
		goto failed;
	}

	if (ctx->in->tokens[i].type == BM_TOK_IMPORT)
	{
		i++;

		while (i < ctx->in->nb_tokens)
		{
			parser_node_init(&use, BM_AST_NODE_USE);
			use.dtor = use_dtor;
			use.debug_data = use_debug_data;

			result = clone_class_path(&class_path, &use.data.as_use.path);
			if (result < 0)
				goto failed;

			use.data.as_use.name = NULL;
			use.data.as_use.alias_name = NULL;

			parser_ignore_eols(ctx, &i);

			if (ctx->in->tokens[i].type != BM_TOK_IDENTIFIER)
				goto failed;

			use.data.as_use.name = u_strdup(ctx->in->tokens[i].data.as_str);
			if (!use.data.as_use.name)
				goto failed;
			i++;

			if (ctx->in->tokens[i].type == BM_TOK_AS)
			{
				i++;

				if (ctx->in->tokens[i].type != BM_TOK_IDENTIFIER)
					goto failed;

				use.data.as_use.alias_name = u_strdup(ctx->in->tokens[i].data.as_str);
				if (!use.data.as_use.alias_name)
					goto failed;
				i++;
			}

			parser_node_append_child(&uses, &use);
			parser_node_init(&use, BM_AST_NODE_UNKNOWN);

			if (ctx->in->tokens[i].type != BM_TOK_COMMA)
				break;
			i++;

			parser_ignore_eols(ctx, &i);
		}
	}
	else
	{
		parser_node_init(&use, BM_AST_NODE_USE);
		use.dtor = use_dtor;
		use.debug_data = use_debug_data;

		result = clone_class_path(&class_path, &use.data.as_use.path);
		if (result < 0)
			goto failed;

		use.data.as_use.name = NULL;
		use.data.as_use.alias_name = NULL;

		parser_node_append_child(&uses, &use);
		parser_node_init(&use, BM_AST_NODE_UNKNOWN);
	}

	destroy_class_path(&class_path);

	if (ctx->in->tokens[i].type != BM_TOK_EOL
			&& ctx->in->tokens[i].type != BM_TOK_SEMICOLON)
	{
		printf("Expected newline or ';' at the end of the use directive, got %s.\n", debug_token_type(ctx->in->tokens[i].type));
		goto failed;
	}
	i++;

	for (unsigned long i = 0; i < parser_node_get_children_count(&uses); i++)
	{
		struct bm_ast_node *use;

		use = parser_node_get_child(&uses, i);
		parser_node_append_child(&ctx->ast.uses, use);
	}

	parser_node_destroy_no_recurse(&uses);

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&use);
	parser_node_destroy(&uses);
	return -1;
}

static int find_alias_generic_param_match(const char *name, struct bm_ast_type *type)
{
	int result;

	for (unsigned long i = 0; i < type->nb_generic_params; i++)
	{
		if (type->generic_params[i].nb_generic_params)
		{
			result = find_alias_generic_param_match(name, &type->generic_params[i]);
			if (result == 0)
				return 0;
		}

		if (buffer_get_line_count(&type->generic_params[i].class_path.seqs) > 0)
			continue;

		if (strcmp(name, type->generic_params[i].class_name) == 0)
			return 0;
	}

	return -1;
}

static int alias_dtor(struct bm_ast_node *node)
{
	destroy_type(&node->data.as_alias.tgt);
	destroy_type(&node->data.as_alias.src);
	return 0;
}

static int alias_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	const struct bm_ast_node_data_alias *data = &node->data.as_alias;

	printf(",\n%*s\"src\": \"", indent_lvl * 2, "");
	debug_type(&data->src);
	printf("\"");

	printf(",\n%*s\"tgt\": \"", indent_lvl * 2, "");
	debug_type(&data->tgt);
	printf("\"");

	return 0;
}

static int parse_alias(struct parser_ctx *ctx, unsigned long *cur_tok_id)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node alias;

	if (ctx->in->tokens[i].type != BM_TOK_ALIAS)
		return -1;
	i++;

	parser_node_init(&alias, BM_AST_NODE_ALIAS);
	alias.dtor = alias_dtor;
	alias.debug_data = alias_debug_data;

	result = parse_type(ctx, &i, &alias.data.as_alias.tgt);
	if (result < 0)
	{
		printf("Expected a type inside alias directive, got %s.\n", debug_token_type(ctx->in->tokens[i].type));
		return -1;
	}

	if (buffer_get_line_count(&alias.data.as_alias.tgt.class_path.seqs) > 0)
	{
		printf("Expected a short type in alias directive, got a fully-qualified type.\n");
		return -1;
	}

	for (unsigned long j = 0; j < alias.data.as_alias.tgt.nb_generic_params; j++)
	{
		if (buffer_get_line_count(&alias.data.as_alias.tgt.generic_params[j].class_path.seqs) > 0
				|| alias.data.as_alias.tgt.generic_params[j].nb_generic_params > 0)
		{
			printf("Expected a short type in alias directive, got a fully-qualified type.\n");
			return -1;
		}
	}

	if (ctx->in->tokens[i].type != BM_TOK_OP_ASSIGN)
	{
		printf("Expected '=' inside alias directive, got %s.\n", debug_token_type(ctx->in->tokens[i].type));
		return -1;
	}
	i++;

	result = parse_type(ctx, &i, &alias.data.as_alias.src);
	if (result < 0)
	{
		printf("Expected a type inside alias directive, got %s.\n", debug_token_type(ctx->in->tokens[i].type));
		return -1;
	}

	if (ctx->in->tokens[i].type != BM_TOK_EOL
			&& ctx->in->tokens[i].type != BM_TOK_SEMICOLON)
	{
		printf("Expected newline or ';' at the end of the alias directive, got %s.\n", debug_token_type(ctx->in->tokens[i].type));
		return -1;
	}
	i++;

	/* Check for generic params consistency */

	for (unsigned long j = 0; j < alias.data.as_alias.tgt.nb_generic_params; j++)
	{
		for (unsigned long k = j + 1; k < alias.data.as_alias.tgt.nb_generic_params; k++)
		{
			if (strcmp(alias.data.as_alias.tgt.generic_params[j].class_name, alias.data.as_alias.tgt.generic_params[k].class_name) == 0)
			{
				printf("Duplicate generic param in alias directive.\n");
				return -1;
			}
		}
	}

	for (unsigned long j = 0; j < alias.data.as_alias.tgt.nb_generic_params; j++)
	{
		result = find_alias_generic_param_match(alias.data.as_alias.tgt.generic_params[j].class_name, &alias.data.as_alias.src);
		if (result < 0)
		{
			printf("Generic param %s doesn't have any match in alias directive.\n", alias.data.as_alias.tgt.generic_params[j].class_name);
			return -1;
		}
	}

	parser_node_append_child(&ctx->ast.aliases, &alias);
	*cur_tok_id = i;
	return 0;
}

static int debug_annotation_mask(enum bm_ast_annotation_mask mask, int indent_lvl)
{
	const char *sep = "";

	mask &= (1u << BM_AST_ANNOTATION_COUNT) - 1;
	if (!mask)
	{
		printf("[]");
		return 0;
	}

	printf("[");
	indent_lvl++;

	for (enum bm_ast_annotation_type type = 0; type < BM_AST_ANNOTATION_COUNT; type++)
	{
		unsigned int type_mask = 1u << type;

		if (mask < type_mask)
			break;

		if (mask & type_mask)
		{
			printf("%s\n%*s\"%s\"", sep, indent_lvl * 2, "", debug_annotation_type(type));

			sep = ",";
		}
	}

	indent_lvl--;
	printf("\n%*s]", indent_lvl * 2, "");

	return 0;
}

static int parse_annotation(struct parser_ctx *ctx, unsigned long *cur_tok_id)
{
	unsigned long i = *cur_tok_id;

	switch (ctx->in->tokens[i].type)
	{
		case BM_TOK_ANNOTATION:
			printf("FIXME: Custom annotations are not supported yet.\n");
			return -1;

		case BM_TOK_ANNOTATION_AUTHORS:
			printf("FIXME: Authors annotations are not supported yet.\n");
			return -1;

		case BM_TOK_ANNOTATION_DEPRECATED:
			printf("FIXME: Deprecated annotations are not supported yet.\n");
			return -1;

		case BM_TOK_ANNOTATION_EXPLICIT_ORDER:
			ctx->cur_annotations |= BM_AST_ANNOTATION_EXPLICIT_ORDER_BIT;
			break;

		case BM_TOK_ANNOTATION_MAIN:
			ctx->cur_annotations |= BM_AST_ANNOTATION_MAIN_BIT;
			if (ctx->ast.has_main)
			{
				printf("Error: Duplicate @Main annotation in the current module.\n");
				return -1;
			}
			ctx->ast.has_main = 1;
			break;

		case BM_TOK_ANNOTATION_READONLY:
			ctx->cur_annotations |= BM_AST_ANNOTATION_READONLY_BIT;
			break;

		default:
			return -1;
	}
	i++;

	if (ctx->in->tokens[i].type != BM_TOK_EOL)
	{
		printf("Expected newline at the end of the annotation %s, got %s.\n", debug_token_type(ctx->in->tokens[*cur_tok_id].type), debug_token_type(ctx->in->tokens[i].type));
		return -1;
	}
	i++;

	*cur_tok_id = i;
	return 0;
}

int parse_block(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node block;

	if (ctx->in->tokens[i].type != BM_TOK_CURLY_BRACKET_OPEN)
		return -1;
	i++;

	parser_node_init(&block, BM_AST_NODE_BLOCK);

	parser_ignore_eols_or_semicolons(ctx, &i);

	while (i < ctx->in->nb_tokens)
	{
		struct bm_ast_node instr;

		result = parse_instr(ctx, &i, &instr);
		if (result < 0)
			break;
		parser_node_append_child(&block, &instr);

		if (ctx->in->tokens[i].type != BM_TOK_EOL
				&& ctx->in->tokens[i].type != BM_TOK_SEMICOLON)
			break;
		i++;

		parser_ignore_eols_or_semicolons(ctx, &i);
	}

	if (block.children_count < 1)
	{
		struct bm_ast_node noop;

		parser_node_init_instr_noop(&noop);
		parser_node_append_child(&block, &noop);
	}

	if (ctx->in->tokens[i].type != BM_TOK_CURLY_BRACKET_CLOSE)
		goto failed;
	i++;

	*out = block;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&block);
	return -1;
}

static int function_common_dtor(struct bm_ast_node *node)
{
	node->data.as_func.annotations = 0;
	destroy_decl_func(&node->data.as_func.decl);
	return 0;
}

static int function_common_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	const struct bm_ast_node_data_func *data = &node->data.as_func;

	debug_decl_func(&data->decl, indent_lvl);

	printf(",\n%*s\"annotations\": ", indent_lvl * 2, "");
	debug_annotation_mask(data->annotations, indent_lvl);
	printf(",\n%*s\"visibility\": \"%s\"", indent_lvl * 2, "", debug_visibility(data->visibility));

	return 0;
}

static int parse_function_common(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out, struct bm_ast_type *class_type)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node func;
	struct bm_ast_node block;

	parser_node_init(&func, BM_AST_NODE_FUNC);
	func.dtor = function_common_dtor;
	func.debug_data = function_common_debug_data;

	func.data.as_func.annotations = ctx->cur_annotations;

	func.data.as_func.visibility = parser_visibility_from_token_type(ctx->in->tokens[i].type);
	if (func.data.as_func.visibility == BM_AST_VISIBILITY_INVAL)
		return -1;
	i++;

	result = parse_decl_func(ctx, &i, &func.data.as_func.decl, class_type);
	if (result < 0)
		goto failed_decl_func;

	parser_ignore_eols(ctx, &i);

	result = parse_block(ctx, &i, &block);
	if (result < 0)
		goto failed_block;
	parser_node_set_child(&func, BM_AST_NODE_FUNC_CHILD_BLOCK, &block);

	*out = func;

	ctx->cur_annotations = 0;

	*cur_tok_id = i;
	return 0;

failed_block:
	parser_node_destroy(&func);

failed_decl_func:
	return -1;
}

static int parse_function(struct parser_ctx *ctx, unsigned long *cur_tok_id)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node func;

	result = parse_function_common(ctx, &i, &func, NULL);
	if (result < 0)
		return -1;

	parser_node_append_child(&ctx->ast.funcs, &func);
	*cur_tok_id = i;
	return 0;
}

static int parse_method(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out, struct bm_ast_type *class_type)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node method;

	result = parse_function_common(ctx, &i, &method, class_type);
	if (result < 0)
		return -1;

	*out = method;

	*cur_tok_id = i;
	return 0;
}

static int prop_dtor(struct bm_ast_node *node)
{
	destroy_decl(&node->data.as_prop.decl);
	return 0;
}

static int prop_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	const struct bm_ast_node_data_prop *data = &node->data.as_prop;

	debug_decl(&data->decl, indent_lvl);

	printf(",\n%*s\"visibility\": \"%s\"", indent_lvl * 2, "", debug_visibility(data->visibility));

	return 0;
}

static int parse_prop(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node prop;

	parser_node_init(&prop, BM_AST_NODE_PROP);
	prop.dtor = prop_dtor;
	prop.debug_data = prop_debug_data;

	prop.data.as_prop.visibility = parser_visibility_from_token_type(ctx->in->tokens[i].type);
	if (prop.data.as_prop.visibility == BM_AST_VISIBILITY_INVAL)
		goto failed;
	i++;

	result = parse_decl(ctx, &i, &prop.data.as_prop.decl);
	if (result < 0)
		goto failed;

	*out = prop;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&prop);
	return -1;
}

static int class_dtor(struct bm_ast_node *node)
{
	destroy_type(&node->data.as_class.type);

	return 0;
}

static int class_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	const struct bm_ast_node_data_class *data = &node->data.as_class;

	printf(",\n%*s\"class_type\": \"", indent_lvl * 2, "");
	debug_type(&data->type);
	printf("\",\n%*s\"annotations\": ", indent_lvl * 2, "");
	debug_annotation_mask(data->annotations, indent_lvl);
	printf(",\n%*s\"visibility\": \"%s\"", indent_lvl * 2, "", debug_visibility(data->visibility));

	return 0;
}

static int parse_class(struct parser_ctx *ctx, unsigned long *cur_tok_id)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node nclass;
	struct bm_ast_node props;
	struct bm_ast_node methods;

	parser_node_init(&nclass, BM_AST_NODE_CLASS);
	nclass.dtor = class_dtor;
	nclass.debug_data = class_debug_data;

	parser_node_init_unknown(&props);
	parser_node_init_unknown(&methods);

	nclass.data.as_class.annotations = ctx->cur_annotations;

	nclass.data.as_class.visibility = parser_visibility_from_token_type(ctx->in->tokens[i].type);
	if (nclass.data.as_class.visibility == BM_AST_VISIBILITY_INVAL)
		goto failed;
	i++;

	if (ctx->in->tokens[i].type != BM_TOK_CLASS)
		goto failed;
	i++;

	result = parse_type(ctx, &i, &nclass.data.as_class.type);
	if (result < 0)
		goto failed;

	if (buffer_get_line_count(&nclass.data.as_class.type.class_path.seqs) > 0)
		goto failed;

	parser_ignore_eols(ctx, &i);

	if (ctx->in->tokens[i].type != BM_TOK_CURLY_BRACKET_OPEN)
		goto failed;
	i++;

	parser_ignore_eols_or_semicolons(ctx, &i);

	while (i < ctx->in->nb_tokens)
	{
		unsigned long i_backup = i;
		struct bm_ast_node prop;

		result = parse_prop(ctx, &i, &prop);
		if (result < 0)
			break;

		if (ctx->in->tokens[i].type != BM_TOK_EOL
				&& ctx->in->tokens[i].type != BM_TOK_SEMICOLON
				&& ctx->in->tokens[i].type != BM_TOK_CURLY_BRACKET_CLOSE)
		{
			parser_node_destroy(&prop);

			i = i_backup;
			break;
		}

		parser_node_append_child(&props, &prop);

		if (ctx->in->tokens[i].type == BM_TOK_CURLY_BRACKET_CLOSE)
			break;
		i++;

		parser_ignore_eols_or_semicolons(ctx, &i);
	}

	while (i < ctx->in->nb_tokens)
	{
		struct bm_ast_node method;

		result = parse_method(ctx, &i, &method, &nclass.data.as_class.type);
		if (result < 0)
			break;

		parser_node_append_child(&methods, &method);

		if (ctx->in->tokens[i].type != BM_TOK_EOL
				&& ctx->in->tokens[i].type != BM_TOK_SEMICOLON)
			break;
		i++;

		parser_ignore_eols_or_semicolons(ctx, &i);
	}

	if (ctx->in->tokens[i].type != BM_TOK_CURLY_BRACKET_CLOSE)
		goto failed;
	i++;

	parser_node_set_child(&nclass, BM_AST_NODE_CLASS_CHILD_PROPS, &props);
	parser_node_set_child(&nclass, BM_AST_NODE_CLASS_CHILD_METHODS, &methods);
	parser_node_append_child(&ctx->ast.classes, &nclass);

	ctx->cur_annotations = 0;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&props);
	parser_node_destroy(&methods);
	parser_node_destroy(&nclass);
	return -1;
}

static int proto_dtor(struct bm_ast_node *node)
{
	destroy_decl_func(&node->data.as_proto.decl);
	return 0;
}

static int proto_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	debug_decl_func(&node->data.as_proto.decl, indent_lvl);

	return 0;
}

static int parse_proto(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out, struct bm_ast_type *interface_type)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node proto;

	parser_node_init(&proto, BM_AST_NODE_PROTO);
	proto.dtor = proto_dtor;
	proto.debug_data = proto_debug_data;

	result = parse_decl_func(ctx, &i, &proto.data.as_proto.decl, interface_type);
	if (result < 0)
		return -1;

	*out = proto;
	*cur_tok_id = i;
	return 0;
}

static int interface_dtor(struct bm_ast_node *node)
{
	destroy_type(&node->data.as_interface.type);

	return 0;
}

static int interface_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	const struct bm_ast_node_data_interface *data = &node->data.as_interface;

	printf(",\n%*s\"interface_type\": \"", indent_lvl * 2, "");
	debug_type(&data->type);
	printf("\",\n%*s\"annotations\": ", indent_lvl * 2, "");
	debug_annotation_mask(data->annotations, indent_lvl);
	printf(",\n%*s\"visibility\": \"%s\"", indent_lvl * 2, "", debug_visibility(data->visibility));

	return 0;
}

static int parse_interface(struct parser_ctx *ctx, unsigned long *cur_tok_id)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node interface;
	unsigned long param_id;

	parser_node_init(&interface, BM_AST_NODE_INTERFACE);
	interface.dtor = interface_dtor;
	interface.debug_data = interface_debug_data;

	interface.data.as_interface.annotations = ctx->cur_annotations;

	interface.data.as_interface.visibility = parser_visibility_from_token_type(ctx->in->tokens[i].type);
	if (interface.data.as_interface.visibility == BM_AST_VISIBILITY_INVAL)
		goto failed;
	i++;

	if (ctx->in->tokens[i].type != BM_TOK_INTERFACE)
		goto failed;
	i++;

	result = parse_type(ctx, &i, &interface.data.as_interface.type);
	if (result < 0)
		goto failed;

	if (buffer_get_line_count(&interface.data.as_interface.type.class_path.seqs) > 0)
		goto failed;

	parser_ignore_eols(ctx, &i);

	if (ctx->in->tokens[i].type != BM_TOK_CURLY_BRACKET_OPEN)
		goto failed;
	i++;

	parser_ignore_eols_or_semicolons(ctx, &i);

	param_id = BM_AST_NODE_INTERFACE_CHILD_PROTO0;

	while (i < ctx->in->nb_tokens)
	{
		struct bm_ast_node proto;

		result = parse_proto(ctx, &i, &proto, &interface.data.as_interface.type);
		if (result < 0)
			break;

		parser_node_set_child(&interface, param_id, &proto);
		param_id++;

		if (ctx->in->tokens[i].type != BM_TOK_EOL
				&& ctx->in->tokens[i].type != BM_TOK_SEMICOLON)
			break;
		i++;

		parser_ignore_eols_or_semicolons(ctx, &i);
	}

	if (ctx->in->tokens[i].type != BM_TOK_CURLY_BRACKET_CLOSE)
		goto failed;
	i++;

	parser_node_append_child(&ctx->ast.interfaces, &interface);

	ctx->cur_annotations = 0;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&interface);
	return -1;
}

int parser(struct lexer_output *in, struct bm_ast *out)
{
	struct parser_ctx ctx;
	int result;
	unsigned long cur_tok_id = 0;

	parser_node_init_unknown(&ctx.ast.aliases);
	parser_node_init_unknown(&ctx.ast.classes);
	parser_node_init_unknown(&ctx.ast.funcs);
	parser_node_init_unknown(&ctx.ast.interfaces);
	parser_node_init_unknown(&ctx.ast.package);
	parser_node_init_unknown(&ctx.ast.uses);
	ctx.ast.has_main = 0;

	ctx.in = in;

	ctx.cur_annotations = 0;
	ctx.last_rshift_was_greater = 0;

	if (!ctx.in->nb_tokens)
	{
		printf("Internal error: The lexer output does not contain any token.\n");
		goto failed;
	}

	if (ctx.in->tokens[ctx.in->nb_tokens - 1].type != BM_TOK_EOF)
	{
		printf("Internal error: The lexer output does not end with EOF.\n");
		goto failed;
	}

	parser_ignore_eols_or_semicolons(&ctx, &cur_tok_id);

	/* Pack must be the first token. */
	result = parse_pack(&ctx, &cur_tok_id);
	if (result < 0)
	{
		printf("No valid pack directive found.\n");
		goto failed;
	}
	parser_ignore_eols_or_semicolons(&ctx, &cur_tok_id);

	/* Then uses. */
	while (cur_tok_id < in->nb_tokens)
	{
		result = parse_use(&ctx, &cur_tok_id);
		if (result < 0)
			break;

		parser_ignore_eols_or_semicolons(&ctx, &cur_tok_id);
	}
	parser_ignore_eols_or_semicolons(&ctx, &cur_tok_id);

	/* Then aliases. */
	while (cur_tok_id < in->nb_tokens)
	{
		result = parse_alias(&ctx, &cur_tok_id);
		if (result < 0)
			break;

		parser_ignore_eols_or_semicolons(&ctx, &cur_tok_id);
	}
	parser_ignore_eols_or_semicolons(&ctx, &cur_tok_id);

	while (cur_tok_id < in->nb_tokens)
	{
		while (cur_tok_id < in->nb_tokens)
		{
			result = parse_annotation(&ctx, &cur_tok_id);
			if (result < 0)
				break;
		}

#if 0
		result = parse_cenum(&ctx);
#endif

		result = parse_class(&ctx, &cur_tok_id);

#if 0
		if (result < 0)
			result = parse_enum(&ctx);
#endif

		if (result < 0)
			result = parse_function(&ctx, &cur_tok_id);

		if (result < 0)
			result = parse_interface(&ctx, &cur_tok_id);

		if (result < 0)
		{
			if (cur_tok_id == ctx.in->nb_tokens - 1
					&& ctx.in->tokens[cur_tok_id].type == BM_TOK_EOF)
				break;
		}

		if (result < 0)
		{
			printf("Unexpected token %s in global namespace.\n", debug_token_type(ctx.in->tokens[cur_tok_id].type));
			goto failed;
		}

		if (ctx.cur_annotations)
		{
			printf("Error: Detached annotation in global space.\n");
			goto failed;
		}

		parser_ignore_eols_or_semicolons(&ctx, &cur_tok_id);
	}

	/* Resolve identifiers & look for conflicts pass. */

	*out = ctx.ast;

	return 0;

failed:
	parser_destroy(&ctx.ast);
	return -1;
}

int parser_debug_ast(struct bm_ast *ast)
{
	printf("===== AST =====\n{\n");

	printf("  \"has_main\": %d,\n", ast->has_main);

	printf("  \"package\": ");
	parser_node_debug(&ast->package, 1);
	printf(",\n");

	printf("  \"uses\": ");
	parser_node_debug(&ast->uses, 1);
	printf(",\n");

	printf("  \"aliases\": ");
	parser_node_debug(&ast->aliases, 1);
	printf(",\n");

	printf("  \"classes\": ");
	parser_node_debug(&ast->classes, 1);
	printf(",\n");

	printf("  \"funcs\": ");
	parser_node_debug(&ast->funcs, 1);
	printf(",\n");

	printf("  \"interfaces\": ");
	parser_node_debug(&ast->interfaces, 1);
	printf("\n");

	printf("}\n===== AST END =====\n");

	return 0;
}

int parser_destroy(struct bm_ast *ast)
{
	parser_node_destroy(&ast->aliases);
	parser_node_destroy(&ast->classes);
	parser_node_destroy(&ast->funcs);
	parser_node_destroy(&ast->interfaces);
	parser_node_destroy(&ast->package);
	parser_node_destroy(&ast->uses);
	ast->has_main = 0;

	return 0;
}
