/*
 * Copyright (C) 2017-2018 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef PARSER_H
#define PARSER_H 1

#include <wchar.h>

#include "ast.h"
#include "lexer.h"

extern int parser_node_init(struct bm_ast_node *node, enum bm_ast_node_type type);
extern unsigned long parser_node_get_children_count(struct bm_ast_node *node);
extern struct bm_ast_node *parser_node_get_child(struct bm_ast_node *node, unsigned long id);
extern int parser_node_set_child(struct bm_ast_node *node, unsigned long id, struct bm_ast_node *child);
extern int parser_node_append_child(struct bm_ast_node *node, struct bm_ast_node *child);
extern struct bm_ast_node *parser_node_exchange_with_child(struct bm_ast_node *node, unsigned long child_id, unsigned long child_sub_node_id);
extern int parser_node_destroy(struct bm_ast_node *node);
extern int parser(struct lexer_output *in, struct bm_ast *out);
extern int parser_debug_ast(struct bm_ast *ast);
extern int parser_destroy(struct bm_ast *ast);

#endif /* PARSER_H */
