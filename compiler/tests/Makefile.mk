SUBDIRS-y :=
EXTDIRS-y :=

tgt := bmc.ok
cmd := $(MKS_TOP_BUILDDIR)/compiler/bmc$(EXE_EXT)

TESTS-y := $(tgt)
DEPS-$(tgt)-y := $(cmd)
CMD-$(tgt)-y := $(cmd)
UNITS-$(tgt)-y := \
	bm/tests/package.bm.ok \
	bm/tests/uses.bm.ok \
	bm/tests/aliases.bm.ok \
	bm/tests/aliases_wrong_class_name.bm.notok \
	bm/tests/classes.bm.ok \
	bm/tests/functions.bm.ok \
	bm/tests/ternary_cond.bm.ok \
	bm/tests/interfaces.bm.ok
