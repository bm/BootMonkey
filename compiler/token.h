/*
 * Copyright (C) 2017-2020 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef TOKEN_H
#define TOKEN_H 1

enum bm_token_type
{
	BM_TOK_UNKNOWN = 0,
	BM_TOK_ABSTRACT,
	BM_TOK_ALIAS,
	BM_TOK_ANNOTATION,
	BM_TOK_ANNOTATION_AUTHORS,
	BM_TOK_ANNOTATION_DEPRECATED,
	BM_TOK_ANNOTATION_MAIN,
	BM_TOK_ANNOTATION_READONLY,
	BM_TOK_ARROW,
	BM_TOK_AS,
	BM_TOK_BRACKET_CLOSE,
	BM_TOK_BRACKET_OPEN,
	BM_TOK_BREAK,
	BM_TOK_CENUM,
	BM_TOK_CLASS,
	BM_TOK_COLON,
	BM_TOK_COMMA,
	BM_TOK_COMMENT,
	BM_TOK_CONST,
	BM_TOK_CONTINUE,
	BM_TOK_CURLY_BRACKET_CLOSE,
	BM_TOK_CURLY_BRACKET_OPEN,
	BM_TOK_DO,
	BM_TOK_DOT,
	BM_TOK_ELLIPSIS,
	BM_TOK_ELSE,
	BM_TOK_ENUM,
	BM_TOK_EOF,
	BM_TOK_EOL,
	BM_TOK_EXTENDS,
	BM_TOK_FALSE,
	BM_TOK_FINAL,
	BM_TOK_FOR,
	BM_TOK_IF,
	BM_TOK_IDENTIFIER,
	BM_TOK_IDENTIFIER_LALNUM,
	BM_TOK_IDENTIFIER_LSNAKE,
	BM_TOK_IDENTIFIER_UCAMEL,
	BM_TOK_IMM_CHAR,
	BM_TOK_IMM_FLOAT,
	BM_TOK_IMM_INT,
	BM_TOK_IMM_STR,
	BM_TOK_IMPLEMENTS,
	BM_TOK_IMPORT,
	BM_TOK_INTERFACE,
	BM_TOK_LOOP,
	BM_TOK_MATCH,
	BM_TOK_NEW,
	BM_TOK_NULL,
	BM_TOK_OP_ADD,
	BM_TOK_OP_AND,
	BM_TOK_OP_ASSIGN,
	BM_TOK_OP_ASSIGN_ADD,
	BM_TOK_OP_ASSIGN_BITWISE_AND,
	BM_TOK_OP_ASSIGN_BITWISE_OR,
	BM_TOK_OP_ASSIGN_BITWISE_XOR,
	BM_TOK_OP_ASSIGN_DIV,
	BM_TOK_OP_ASSIGN_MUL,
	BM_TOK_OP_ASSIGN_MOD,
	BM_TOK_OP_ASSIGN_POW,
	BM_TOK_OP_ASSIGN_SHIFT_LEFT,
	BM_TOK_OP_ASSIGN_SHIFT_RIGHT,
	BM_TOK_OP_ASSIGN_SUB,
	BM_TOK_OP_BITWISE_AND,
	BM_TOK_OP_BITWISE_NOT,
	BM_TOK_OP_BITWISE_OR,
	BM_TOK_OP_BITWISE_XOR,
	BM_TOK_OP_DEC,
	BM_TOK_OP_DIV,
	BM_TOK_OP_EQUAL,
	BM_TOK_OP_GEQ,
	BM_TOK_OP_GREATER,
	BM_TOK_OP_INC,
	BM_TOK_OP_LEQ,
	BM_TOK_OP_LOWER,
	BM_TOK_OP_MOD,
	BM_TOK_OP_MUL,
	BM_TOK_OP_NEQ,
	BM_TOK_OP_NOT,
	BM_TOK_OP_OR,
	BM_TOK_OP_POW,
	BM_TOK_OP_SHIFT_LEFT,
	BM_TOK_OP_SHIFT_RIGHT,
	BM_TOK_OP_TO_NONNULLABLE,
	BM_TOK_OP_SUB,
	BM_TOK_OP_XOR,
	BM_TOK_PACK,
	BM_TOK_PARENT,
	BM_TOK_PRIVATE,
	BM_TOK_PROTECTED,
	BM_TOK_PUBLIC,
	BM_TOK_QUESTION_MARK,
	BM_TOK_RETURN,
	BM_TOK_SEMICOLON,
	BM_TOK_SQUARE_BRACKET_CLOSE,
	BM_TOK_SQUARE_BRACKET_OPEN,
	BM_TOK_STATIC,
	BM_TOK_THIS,
	BM_TOK_TILL,
	BM_TOK_TO,
	BM_TOK_TRUE,
	BM_TOK_USE,
	BM_TOK_VAR,
	BM_TOK_WHILE
};

union bm_token_data
{
	int as_int;
	float as_float;
	char as_char;
	char *as_str;
};

struct bm_token
{
	enum bm_token_type type;
	size_t off;
	size_t lineno;
	size_t colno;
	union bm_token_data data;
};

#endif /* TOKEN_H */
