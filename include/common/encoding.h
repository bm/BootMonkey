/*
 * Copyright (C) 2015-2018 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef COMMON_ENCODING_H
#define COMMON_ENCODING_H 1

#include <stdint.h>

static inline uint8_t read_u8(uint8_t *src) {
	return src[0];
}

static inline uint16_t read_u16(uint8_t *src) {
	uint16_t n = 0;

	n |= (uint16_t)src[0];
	n |= ((uint16_t)src[1]) << 8;

	return n;
}

static inline uint32_t read_u32(uint8_t *src) {
	uint32_t n = 0;

	n |= (uint32_t)src[0];
	n |= ((uint32_t)src[1]) << 8;
	n |= ((uint32_t)src[2]) << 16;
	n |= ((uint32_t)src[3]) << 24;

	return n;
}

static inline uint64_t read_u64(uint8_t *src) {
	uint64_t n = 0;

	n |= (uint64_t)src[0];
	n |= ((uint64_t)src[1]) << 8;
	n |= ((uint64_t)src[2]) << 16;
	n |= ((uint64_t)src[3]) << 24;
	n |= ((uint64_t)src[4]) << 32;
	n |= ((uint64_t)src[5]) << 40;
	n |= ((uint64_t)src[6]) << 48;
	n |= ((uint64_t)src[7]) << 56;

	return n;
}

static inline void write_u8(uint8_t *dst, uint8_t n) {
	dst[0] = n;
}

static inline void write_u16(uint8_t *dst, uint16_t n) {
	dst[0] = (uint8_t)(n & 0xFF);
	dst[1] = (uint8_t)((n >> 8) & 0xFF);
}

static inline void write_u32(uint8_t *dst, uint32_t n) {
	dst[0] = (uint8_t)(n & 0xFF);
	dst[1] = (uint8_t)((n >> 8)  & 0xFF);
	dst[2] = (uint8_t)((n >> 16) & 0xFF);
	dst[3] = (uint8_t)((n >> 24) & 0xFF);
}

static inline void write_u64(uint8_t *dst, uint64_t n) {
	dst[0] = (uint8_t)(n & 0xFF);
	dst[1] = (uint8_t)((n >> 8)  & 0xFF);
	dst[2] = (uint8_t)((n >> 16) & 0xFF);
	dst[3] = (uint8_t)((n >> 24) & 0xFF);
	dst[4] = (uint8_t)((n >> 32) & 0xFF);
	dst[5] = (uint8_t)((n >> 40) & 0xFF);
	dst[6] = (uint8_t)((n >> 48) & 0xFF);
	dst[7] = (uint8_t)((n >> 56) & 0xFF);
}

#endif /* COMMON_ENCODING_H */
