/*
 * Copyright (C) 2017-2019 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef UTIL_FILE_H
#define UTIL_FILE_H 1

#include <stddef.h>
#include <stdio.h>
#include <stdint.h>

#define U_FILE_SIZE_INVAL (SIZE_MAX)

enum u_file_open_mode
{
	U_FILE_OPEN_READ  = 1 << 0,
	U_FILE_OPEN_WRITE = 1 << 1,
	U_FILE_OPEN_BIN   = 1 << 2
};

struct u_file
{
	FILE *fp;
	enum u_file_open_mode mode;
	size_t size;
};


int u_file_open(struct u_file *file, const char *path, enum u_file_open_mode mode);
int u_file_import(struct u_file *file, FILE *fp, enum u_file_open_mode mode);
size_t u_file_get_size(struct u_file *file);
void *u_file_get_contents(struct u_file *file, size_t *read_len);
int u_file_close(struct u_file *file);

#endif /* UTIL_FILE_H */
