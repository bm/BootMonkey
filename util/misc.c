/*
 * Copyright (C) 2019-2020 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "misc.h"

size_t u_strcmp_first_mismatch(const char *s1, const char *s2)
{
    size_t i = 0;
    int d;

    while ((d = *s1++ - *s2++) == 0 && s1[-1])
        i++;

    if (d == 0)
        return SIZE_MAX;

    return i;
}

int u_strcmp_start(const char *haystack, const char *needle)
{
    size_t len;

    len = strlen(needle);
    return strncmp(haystack, needle, len);
}

int u_strncmp_start(const char *haystack, const char *needle, size_t n)
{
    size_t len;

    len = strlen(needle);
    len = u_min2_size_t(len, n);
    return strncmp(haystack, needle, len);
}

char *u_strdup(const char *str)
{
    size_t size;
    char *s;

    size = strlen(str) + 1;
    s = malloc(size);
    if (!s)
        return NULL;

    memcpy(s, str, size);
    return s;
}

char *u_strndup(const char *str, size_t n)
{
    size_t len;
    char *s;

    len = u_strnlen(str, n);
    s = malloc(len + 1);
    if (!s)
        return NULL;

    memcpy(s, str, len);
    s[len] = '\0';
    return s;
}

size_t u_strnlen(const char *str, size_t max_len)
{
    const void *nilp;

    nilp = memchr(str, '\0', max_len);
    if (!nilp)
        return max_len;

    return (size_t)nilp - (size_t)str;
}
