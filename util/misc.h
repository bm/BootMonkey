/*
 * Copyright (C) 2019-2020 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef UTIL_MISC_H
#define UTIL_MISC_H 1

#include <stddef.h>
#include <stdint.h>

static inline size_t u_min2_size_t(size_t a, size_t b) {
    return a < b ? a : b;
}

static inline size_t u_max2_size_t(size_t a, size_t b) {
    return a > b ? a : b;
}

size_t u_strcmp_first_mismatch(const char *s1, const char *s2);
int u_strcmp_start(const char *haystack, const char *needle);
char *u_strdup(const char *str);
char *u_strndup(const char *str, size_t size);
size_t u_strnlen(const char *str, size_t max_len);

#endif /* UTIL_MISC_H */
